import os
import time
import psutil
import datetime
from tabulate import tabulate
import logging
from logging.handlers import RotatingFileHandler

# Set up the logger
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('detailed_logger')

class Process:
    def __init__(self, pid, name, create_time, cpu_usage, cpu_affinity, status, memory, user):
        self.pid = pid
        self.name = name
        self.create_time = create_time
        self.cpu_usage = cpu_usage
        self.cpu_affinity = cpu_affinity
        self.status = status
        self.memory = memory
        self.user = user

    def get_size(self, bytes):
        for i in ['', 'K', 'M', 'G', 'T', 'P', 'E']:
            if bytes < 1024:
                return f"{bytes:.2f}{i}B"
            bytes /= 1024

class ProcessManager:
    def __init__(self):
        self.processes = []

    def get_processes(self):
        for p in psutil.process_iter():
            with p.oneshot():
                pid =p.pid
                if pid == 0:
                    continue
                name = p.name()
                try:
                    create_time = datetime.datetime.fromtimestamp(p.create_time())
                except OSError:
                    create_time = datetime.datetime.fromtimestamp(psutil.boot_time())
                cpu_usage = p.cpu_percent()
                try:
                    cpu_affinity = len(p.cpu_affinity())
                except psutil.AccessDenied:
                    cpu_affinity = 0
                status = p.status()
                try:
                    memory = p.memory_full_info().uss
                except psutil.AccessDenied:
                    memory = 0
                try:
                    user = p.username()
                except psutil.AccessDenied:
                    user = "Na"
            self.processes.append(Process(pid, name, create_time, cpu_usage, cpu_affinity, status, memory, user))

    def print_processes(self):
        print(tabulate([p.__dict__ for p in self.processes], headers="keys",tablefmt='github'))

if __name__ == "__main__":
    # Create a file handler that logs all messages with a level of DEBUG or higher
    file_handler = RotatingFileHandler('detailed_log.txt', maxBytes=1024*1024, backupCount=5)
    file_handler.setLevel(logging.DEBUG)

    # Create a formatter that includes the timestamp, log level, and message
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)

    # Add the file handler to the logger
    logger.addHandler(file_handler)

    pm = ProcessManager()
    while True:
        pm.get_processes()
        pm.print_processes()

        # Log the current time and number of processes
        logger.debug(f"Current time: {datetime.datetime.now()}, Number of processes: {len(pm.processes)}")

        time.sleep(1)
        if "nt" in os.name:
            os.system("cls")
        else:
            os.system("clear")
