import unittest
from MonitoringOBB import get_size

class TestGetSize(unittest.unittest):

    def test_get_size(self):
        self.assertEqual(get_size(0), '0.00B')
        self.assertEqual(get_size(1), '1.00B')
        self.assertEqual(get_size(1024), '1.00K')
        self.assertEqual(get_size(1024 * 1024), '1.00M')
        self.assertEqual(get_size(1024 * 1024 * 1024), '1.00G')
        self.assertEqual(get_size(1024 * 1024 * 1024 * 1024), '1.00T')
        self.assertEqual(get_size(1024 * 1024 * 1024 * 1024 * 1024), '1.00P')
        self.assertEqual(get_size(1024 * 1024 * 1024 * 1024 * 1024 * 1024), '1.00E')

if __name__ == '__main__':
    unittest.unittest()
