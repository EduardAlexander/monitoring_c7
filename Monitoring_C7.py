import psutil
import time
import logging
import matplotlib.pyplot as plt

# Logging einrichten
logging.basicConfig(filename='monitoring.log', level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(message)s')

# Globale Variablen für die Diagramm-Daten
timestamps = []  # Liste für Zeitstempel
cpu_usages = []  # Liste für CPU-Auslastung
memory_usages = []  # Liste für Speicherauslastung
disk_usages = []  # Liste für Festplattenauslastung

# Setzen des CPU-Auslastungsschwellenwerts
cpu_threshold = 30

def get_cpu_usage():
    """
    Aktuelle CPU-Auslastung in Prozent abrufen.

    Returns:
        float: CPU-Auslastung in Prozent.
    """
    return psutil.cpu_percent(interval=1)

def get_memory_usage():
    """
    Aktuelle Speichernutzungsinformationen abrufen.

    Returns:
        tuple: Tuple mit Speicherauslastung in Prozent, genutztem Speicher in GB und Gesamtspeicher in GB.
    """
    memory_info = psutil.virtual_memory()
    return memory_info.percent, memory_info.used / (1024 ** 3), memory_info.total / (1024 ** 3)

def get_disk_usage():
    """
    Aktuelle Festplattennutzungsinformationen für das Wurzelverzeichnis abrufen.

    Returns:
        tuple: Tuple mit Festplattenauslastung in Prozent, genutztem Festplattenspeicher in GB und Gesamtfestplattenspeicher in GB.
    """
    disk_info = psutil.disk_usage('/')
    return disk_info.percent, disk_info.used / (1024 ** 3), disk_info.total / (1024 ** 3)

def get_running_processes_count():
    """
    Anzahl der aktuell laufenden Prozesse abrufen.

    Returns:
        int: Anzahl der laufenden Prozesse.
    """
    return len(list(psutil.process_iter()))

def log_to_file(message):
    """
    Eine Nachricht in die im Logging konfigurierte Datei protokollieren.

    Args:
        message (str): Die zu protokollierende Nachricht.
    """
    logging.info(message)

def plot_performance_data():
    """
    Diagramme für CPU-, Speicher- und Festplattenauslastung über die Zeit erstellen.
    """
    plt.figure(figsize=(10, 6))

    plt.subplot(3, 1, 1)
    plt.plot(timestamps, cpu_usages, label='CPU-Auslastung')
    plt.title('CPU-Auslastung über die Zeit')
    plt.xlabel('Zeit (s)')
    plt.ylabel('CPU-Auslastung (%)')
    plt.legend()

    plt.subplot(3, 1, 2)
    plt.plot(timestamps, memory_usages, label='Speicherauslastung')
    plt.title('Speicherauslastung über die Zeit')
    plt.xlabel('Zeit (s)')
    plt.ylabel('Speicherauslastung (%)')
    plt.legend()

    plt.subplot(3, 1, 3)
    plt.plot(timestamps, disk_usages, label='Festplattenauslastung')
    plt.title('Festplattenauslastung über die Zeit')
    plt.xlabel('Zeit (s)')
    plt.ylabel('Festplattenauslastung (%)')
    plt.legend()

    plt.tight_layout()
    plt.show()

def main():
    try:
        while True:
            timestamp = time.time()
            timestamps.append(timestamp)

            cpu_usage = get_cpu_usage()
            memory_usage_percent, memory_usage_used_gb, memory_usage_total_gb = get_memory_usage()
            disk_usage_percent, disk_usage_used_gb, disk_usage_total_gb = get_disk_usage()
            processes_count = get_running_processes_count()

            cpu_usages.append(cpu_usage)
            memory_usages.append(memory_usage_percent)
            disk_usages.append(disk_usage_percent)

            log_message = (f"Zeitstempel: {time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(timestamp))} | "
                           f"CPU-Auslastung: {cpu_usage}% | Speicherauslastung: {memory_usage_percent}% "
                           f"({memory_usage_used_gb:.2f}GB / {memory_usage_total_gb:.2f}GB) "
                           f"| Festplattenauslastung: {disk_usage_percent}% "
                           f"({disk_usage_used_gb:.2f}GB / {disk_usage_total_gb:.2f}GB) | "
                           f"Lauftende Prozesse: {processes_count}")

            log_to_file(log_message)

            print(log_message)

            # Überprüfen, ob die CPU-Auslastung den Schwellenwert überschreitet
            if cpu_usage > cpu_threshold:
                warning_message = (f"Warnung: CPU-Auslastung ({cpu_usage}%) überschreitet den Schwellenwert ({cpu_threshold}%)")
                log_to_file(warning_message)
                print(warning_message)

            time.sleep(1)

    except KeyboardInterrupt:
        print("Überwachung wurde vom Benutzer gestoppt.")
        # Schließe das Plot-Fenster aus, um zu verhindern, dass es in die Log-Datei geloggt wird
        plt.ioff()
        plot_performance_data()
        plt.ion()

if __name__ == "__main__":
    main()
